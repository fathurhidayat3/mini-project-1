import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Base from './components/Base/Base';

ReactDOM.render(<Base />, document.getElementById('root'));
