import React from "react";

const PanelHeader = ({title, number, author}) => {
    return <div className="panel-header">
        <h2>{title}
            {number && <span> #{number}</span>}
        </h2>
        {author && <p>by 
            : <span className="author-login">{author.login}</span>
            </p>}
    </div>
}

export default PanelHeader;