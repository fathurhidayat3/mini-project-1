import React from "react";
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

import Navbar from "../Navbar/Navbar";
import PanelHeader from "../PanelHeader/PanelHeader";

class DetailIssue extends React.Component {
    render() {
        const { location } = this.props;
        const queryParam = location.query;

        const GET_ISSUE_DETAIL = gql`
        {
            getDetailIssue(id: ${queryParam
                ? queryParam.issueNumber
                : this.props.history.push("/")
            }) {
                number
                title
                author {
                    login
                }
                comments {
                    nodes {
                        author {
                            login
                        }
                        body
                    }
                }
            }
        }`;

        return (
            <div>
                <Navbar title="React Repository Issue List" />

                <Query query={GET_ISSUE_DETAIL}>
                    {({ loading, error, data }) => {
                        if (loading) return <div className="empty-state">Loading...</div>;
                        if (error) return <div className="empty-state">Error! ${error.message}</div>;

                        const { comments } = data.getDetailIssue;

                        return (
                            <div>
                                <PanelHeader {...data.getDetailIssue} />

                                {comments.nodes && comments.nodes.length > 0 ?
                                    <ul className="list-group">
                                        {comments.nodes.map((commentItem, index) => {
                                            return <li className="list-item" key={index}>
                                                <div className="author-login">{commentItem.author.login}</div>
                                                <hr />
                                                <div>{commentItem.body}</div>
                                            </li>
                                        })
                                        }
                                    </ul>
                                    : <div className="empty-state">
                                        No comments yet, check back later :)
                                </div>
                                }
                            </div>
                        )
                    }}
                </Query>
            </div>
        )
    }
}

export default DetailIssue;