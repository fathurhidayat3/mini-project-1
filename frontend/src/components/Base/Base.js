import React from "react";
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter, Route } from "react-router-dom";

import Home from "../Home/Home";
import DetailIssue from "../DetailIssue/DetailIssue";

const client = new ApolloClient({
    link: new HttpLink({ uri: "http://localhost:8081/graphql" }),
    cache: new InMemoryCache(),
    // fetchOptions: {
    //     mode: "no-cors"
    // },    
});

class Base extends React.Component {
    render() {
        return (
            <ApolloProvider client={client}>
                <BrowserRouter>
                    <Route path="/" exact component={Home} />
                    <Route path="/issue/:issueNumber" component={DetailIssue} />
                </BrowserRouter>
            </ApolloProvider>
        )
    }
}

export default Base;