import React from "react";
import { Link } from "react-router-dom"


const Navbar = ({ title }) => {
    return <div className="navbar">
        <Link to="/">
            <h3>{title}</h3>
        </Link>
    </div>
}

export default Navbar;