import React from "react";
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { Link } from "react-router-dom"

import Navbar from "../Navbar/Navbar";

class Home extends React.Component {
  render() {
    const GET_ISSUES = gql`
    {
      getIssues{
        number
        title
      }
    }`;

    return (
      <div>
        <Navbar title="React Repository Issue List" />

        <Query query={GET_ISSUES}>
          {({ loading, error, data }) => {
            if (loading) return <div className="empty-state">Loading...</div>;
            if (error) return <div className="empty-state">Error! ${error.message}</div>;

            return (
              <ul className="list-group">
                {
                  data.getIssues.map((issueItem, index) => {
                    return <li className="list-item" key={index}>
                      <Link to={{
                        pathname: `issue/${issueItem.number}`,
                        query: { issueNumber: issueItem.number }
                      }}>
                        {issueItem.title} <span>#{issueItem.number}</span>
                      </Link>
                    </li>
                  })
                }
              </ul>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default Home;