const express = require("express");
const graphqlHTTP = require("express-graphql");
const { buildSchema } = require("graphql");
const axios = require("axios");
const cors = require("cors");

axios.defaults.headers.common = {
  "Content-Type": 'application/json',
  "Accept": 'application/json',
  Authorization: "Bearer bd17de47fa3e7f15726ef6ad8c34957e14a6cf6b"
};

const app = express();

const schema = buildSchema(`
  type Issue {
    number: Int!
    title: String!
  } 

  type DetailIssue {
    number: Int!
    title: String!
    author: Author!
    comments: CommentNode!
  }

  type Author {
    login: String!
  }

  type CommentNode {
    nodes: [Comment]
  }

  type Comment {
    author: Author!
    body: String!
  }

  type Query {
      getIssues: [Issue!]
      getDetailIssue(id: Int!): DetailIssue!
  }
`)

const root = {
  getIssues: () => {
    const queryIssues = `query { 
      repository(owner: "facebook", name: "react") {
        issues(first: 20){
          nodes {
            number
            title
          }
        }
      }
    }`

    return axios.post(
      "https://api.github.com/graphql",
      { query: queryIssues }
    )
      .then(res => res.data.data.repository.issues.nodes)
    // 
  },
  getDetailIssue: ({ id }) => {
    const queryDetailIssue = `query { 
      repository(owner: "facebook", name: "react"){
        issue(number: ${id}){
          number
          title
          author {
            login
          }
          comments(first: 10) {
            nodes {
              author {
                login
              }
              body
            }
          }
        }
      }
    }`

    return axios.post(
      "https://api.github.com/graphql",
      { query: queryDetailIssue }
    )
      .then(res => res.data.data.repository.issue)
  }
}

const corsOptions = {
  origin: "http://localhost"
}

app.use(cors());
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true
}));
app.listen(8080);

console.log("App running in port 8080");