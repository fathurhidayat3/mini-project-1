# Mini Proyek 1 

Repository ini berisi mini proyek ke-1 (untuk kumparan academy) : consume GraphQL API dari Github menggunakan aplikasi backend (expressJs) dan mengirimkan/menyediakan data tersebut untuk di consume oleh aplikasi frontend (ReactJs) yang telah dikemas dengan docker.

## Instalasi

### Menggunakan `docker-compose`
1. Pastikan Anda telah menginstall docker client di local machine.
2. Clone repository menggunakan :
```bash
git clone https://gitlab.com/fathurhidayat3/mini-project-1
```
3. Masuk ke directory dan jalankan :
```bash
docker-compose build
```
4. Akses aplikasi frontend di alamat : `http://localhost:80`
5. Akses graphql sandbox di alamat : `http://localhost:8081`

### Menggunakan `docker pull` untuk masing-masing image (frontend dan backend)
1. Pull frontend image :
```bash
docker pull registry.gitlab.com/fathurhidayat3/mini-project-1:frontend
```
2. Pull backend image :
```bash
docker pull registry.gitlab.com/fathurhidayat3/mini-project-1:backend
```
3. Buat container untuk frontend dengan :
```bash
docker create -p 80:8080 --name fe-container registry.gitlab.com/fathurhidayat3/mini-project-1:frontend
```
4. Buat container untuk backend dengan :
```bash
docker create -p 8081:8080 --name be-container registry.gitlab.com/fathurhidayat3/mini-project-1:backend
```
5. Jalankan masing-masing container :
```bash
docker start be-container
```
dan 
```bash
docker start fe-container
```
6. Akses aplikasi frontend di alamat : `http://localhost:80`
7. Akses graphql sandbox di alamat : `http://localhost:8081`


## Lisensi
[MIT](https://choosealicense.com/licenses/mit/)